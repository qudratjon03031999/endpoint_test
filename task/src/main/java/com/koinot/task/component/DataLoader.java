package com.koinot.task.component;

import com.koinot.task.entity.*;
import com.koinot.task.enums.RoleName;
import com.koinot.task.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */
@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.sql.init.mode}")
    private String initialMode;

    @Autowired
    UserRepository userRepository;


    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Override
    public void run(String... args) {

        if (initialMode.equals("always")) {



            roleRepository.deleteAll();

            List<Role> save = new ArrayList<>();

            for (RoleName name : RoleName.values()) {

                save.add(roleRepository.save(new Role(name)));

            }

            userRepository.deleteAll();
            for (int i = 0; i < 5; i++) {
                User user = new User();
                user.setPhoneNumber("+99891772" + i);
                user.setLastName("Qudratjon" + i);
                user.setFirstName("Komilov" + i);
                user.setPassword(passwordEncoder.encode("koinot" + i));
                user.setUsername("username" + i);
                user.setAddress("Toshkent shahar oybek metro" + i);
                user.setLatitude(13249847987D + i);
                user.setLongitude(49846846814D + i);
                user.setBalance(123000D + i);
                user.setRoles(List.of(save.get(i % 2)));
                userRepository.save(user);
            }

            User user = new User();
            user.setPhoneNumber("+998917797278");
            user.setLastName("Qudratjon");
            user.setFirstName("Komilov");
            user.setPassword(passwordEncoder.encode("koinot"));
            user.setUsername("koinot_admin");
            user.setAddress("Toshkent shahar oybek metro");
            user.setLatitude(13249847987D);
            user.setLongitude(49846846814D);
            user.setBalance(123000D);
            user.setRoles(save);
            User save1 = userRepository.save(user);


        }
    }

}
