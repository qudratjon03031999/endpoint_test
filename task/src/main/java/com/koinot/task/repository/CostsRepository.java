package com.koinot.task.repository;

import com.koinot.task.entity.Costs;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */
@RepositoryRestResource(collectionResourceRel = "costs", path = "costs")
public interface CostsRepository extends PagingAndSortingRepository<Costs, Integer> {

}
