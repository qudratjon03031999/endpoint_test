package com.koinot.task.repository;

import com.koinot.task.entity.Role;
import com.koinot.task.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */

public interface RoleRepository extends JpaRepository<Role, Integer> {

    List<Role> findAllByName(RoleName name);

    Role findByName(RoleName name);

    List<Role> findAllByNameIn(List<RoleName> names);
}
