package com.koinot.task.repository;

import com.koinot.task.entity.Groups;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */
@RepositoryRestResource(collectionResourceRel = "groups", path = "groups")
public interface GroupsRepository extends PagingAndSortingRepository<Groups, Integer> {

}
