package com.koinot.task.repository;

import com.koinot.task.entity.Costs;
import com.koinot.task.entity.Groups;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */
@Repository
public interface CostRepository extends JpaRepository<Costs, Integer> {

}
