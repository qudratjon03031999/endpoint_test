package com.koinot.task.repository;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */
public interface StatisticByRegion {
    String getRegion();
    int getCount();
}
