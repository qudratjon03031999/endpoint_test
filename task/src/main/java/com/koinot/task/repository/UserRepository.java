package com.koinot.task.repository;

import com.koinot.task.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {



    @Query(value="SELECT * FROM (SELECT s.region,COUNT(s)FROM users s GROUP BY region(s) ORDER BY region(s)) k WHERE k.count>1 ORDER BY k.count DESC", nativeQuery=true)
    List<StatisticByRegion> generationStatistic();

    List<User> findAllByRegion(String region);

    Page<User> findAllByPhoneNumberIgnoreCaseContainingAndFirstNameIgnoreCaseContainingAndLastNameIgnoreCaseContaining(String phoneNumber,
                                                                                                                       String firstName, String lastName,
                                                                                                                       Pageable pageable);

    Page<User> findByRolesId(Integer roles_id, Pageable pageable);

    Optional<User> findByPhoneNumber(String phoneNumber);


    boolean existsByPhoneNumberAndIdNot(String phoneNumber, Integer id);


    boolean existsAllByPhoneNumber(String phoneNumber);

    User findAllByPhoneNumber(String phoneNumber);


    boolean existsByPhoneNumber(String phoneNumber);

    boolean existsByEmail(String email);

    boolean existsByEmailAndIdNot(String email, Integer id);

    boolean existsByPassportNumber(String passportNumber);

    boolean existsByPassportNumberAndIdNot(String passportNumber, Integer id);




}
