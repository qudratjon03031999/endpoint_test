package com.koinot.task.enums;
/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_SUPPER_ADMIN,
    ROLE_MODERATOR,

}
