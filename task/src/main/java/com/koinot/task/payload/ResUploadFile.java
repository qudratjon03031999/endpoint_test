package com.koinot.task.payload;

import com.koinot.task.enums.AttachmentTypeEnumWhy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResUploadFile {
    private Integer fileId;
    private String fileName;
    private AttachmentTypeEnumWhy why;
    private String fileType;
    private long size;
    private String link;
//    private MultipartFile file;
}
