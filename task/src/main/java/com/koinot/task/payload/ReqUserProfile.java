package com.koinot.task.payload;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReqUserProfile {

    private Integer id;

    @NotNull
    @NotBlank()
    @Pattern(regexp = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\\\\\\\s\\\\\\\\./0-9]*$", message = "telephone number is invalid")
    @ApiModelProperty(notes = "phone number ", name = "phoneNumber", required = true, value = "+998917797278")
    private String phoneNumber;

    @NotNull(message = "password must be not null")
    @NotBlank(message = "password is empty")
    @Length(min = 4, max = 30, message = "password is invalid length is min = 4 max = 30")
    @ApiModelProperty(notes = "password", name = "password", required = true, value = "123pas")
    private String password;

    @Pattern(regexp = "^\\d\\d\\d\\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01])$", message = "date Of Birthis invalid")
    @ApiModelProperty(notes = "date of birth", name = "date of birth", required = true, value = "1999-03-03")
    private String dateOfBirth;

    @ApiModelProperty(notes = "email", name = "email", required = true, value = "komilovqudratjon@gmail.com")
    @Email()
    private String email;

    @Pattern(regexp = "^(-?\\d+(\\.\\d+)?)$", message = "longitude is invalid")
    private double longitude;

    @Pattern(regexp = "^(-?\\d+(\\.\\d+)?)$", message = "latitude is invalid")
    private double latitude;

    @ApiModelProperty(notes = "address", name = "address", required = true, value = "Buxoro")
    private String address;

    @ApiModelProperty(notes = "firstName", name = "firstName", required = true, value = "Qudratjon")
    private String firstName;

    @ApiModelProperty(notes = "lastName", name = "lastName", required = true, value = "Komilov")
    private String lastName;

    @Pattern(regexp = "[A-Z]{2}[0-9]{7}", message = "passport number is invalid")
    private String passportNumber;

    private List<ResUploadFile> photo;

    private String username;

    private String language;



}
