package com.koinot.task.payload;

import com.koinot.task.enums.RoleName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JwtResponse {
    private String accessToken;
    private String tokenType = "Bearer";
    private String username;
    private String lastName;
    private String firstName;
    private List<RoleName> roles;

    public JwtResponse(String accessToken, String username, String lastName, String firstName, List<RoleName> roles) {
        this.accessToken = accessToken;
        this.username = username;
        this.lastName = lastName;
        this.firstName = firstName;
        this.roles = roles;
    }
}
