package com.koinot.task.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorsField {
    private String field;
    private String expelling;
}
