package com.koinot.task.payload;

import com.koinot.task.enums.RoleName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReqUserData {

    private Integer id;

    @Pattern(regexp = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\\\\\\\s\\\\\\\\./0-9]*$", message = "telephone number is invalid")
    @ApiModelProperty(notes = "phone number ", name = "phoneNumber", required = true, value = "+998917797278")
    private String phoneNumber;//

    private String ipAddress; //

    private String email; //

    private Date expiredCode;//

    private String verifyCode; //

    private String username; //

    private Double balance; //


    private String firstName;//

    private String lastName;//

    private Date dateOfBirth; // tug'lgan kuni ishlatilmagan

    private String middleName;//

    private String region;

    private String passportNumber;


    private String language; //

    @ApiModelProperty(notes = "address", name = "address", required = true, value = "Buxoro")
    private String address;//

    @Pattern(regexp = "^(-?\\d+(\\.\\d+)?)$", message = "longitude is invalid")
    private Double longitude; //

    @Pattern(regexp = "^(-?\\d+(\\.\\d+)?)$", message = "latitude is invalid")
    private Double latitude;//

    private ResUploadFile photo;//

    private List<RoleName> roles; //


    private boolean accountNonExpired;

    private boolean accountNonLocked;

    private boolean credentialsNonExpired;

    private boolean enabled;



}
