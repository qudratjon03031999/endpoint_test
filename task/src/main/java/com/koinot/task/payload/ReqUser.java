package com.koinot.task.payload;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReqUser {

    @NotNull
    @NotBlank()
    @Pattern(regexp = "^[+][9][9][8][0-9]{9}$", message = "telephone number is invalid")
    @ApiModelProperty(notes = "phone number ", name = "phoneNumber", required = true, value = "+998917797278")
    private String phoneNumber;

    @NotNull(message = "password must be not null")
    @NotBlank(message = "password is empty")
    @Length(min = 4, max = 30, message = "password is invalid length is min = 4 max = 30")
    @Pattern(regexp = "^(?:(?=.*?\\p{N})(?=.*?[\\p{S}\\p{P} ])(?=.*?\\p{Lu})(?=.*?\\p{Ll}))[^\\p{C}]{4,16}$", message = "Uzunligi 8-16 oralig'ida. Parolda kamida bitta katta harf, son va belgi bo'lishi shart.")
    @ApiModelProperty(notes = "password", name = "password", required = true, value = "koinot")
    private String password;

    private String firstName;

    private String lastName;
}
