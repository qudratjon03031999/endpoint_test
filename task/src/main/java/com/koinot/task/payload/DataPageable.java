package com.koinot.task.payload;

import lombok.*;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DataPageable {
    private int number;
    private boolean last;
    private int size;
    private int numberOfElements;
    private int totalPages;
    private Pageable pageable;
    private Sort sort;
    private boolean first;
    private long totalElements;
    private boolean empty;
    private Object content;
}
