package com.koinot.task.payload;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ApiModel("ApiResponseModel")
public class ApiResponseModel {
    @ApiModelProperty(name = "success code", position = 1, example = "200")
    private int success;
    @ApiModelProperty(name = "response message", position = 2, example = "successes")
    private String message;
    @ApiModelProperty(name = "response object", position = 3, example = "null")
    private Object objectKoinot;

    public ApiResponseModel(int success, String message) {
        this.success = success;
        this.message = message;
    }
}
