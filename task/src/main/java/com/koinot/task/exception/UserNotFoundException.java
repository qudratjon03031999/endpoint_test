package com.koinot.task.exception;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ResponseStatus;
/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */
@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class UserNotFoundException extends UsernameNotFoundException {
    public UserNotFoundException(String msg) {
        super( msg );
    }

    public UserNotFoundException(String msg,Throwable t) {
        super( msg,t );
    }
}
