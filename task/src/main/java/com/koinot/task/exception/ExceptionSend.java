package com.koinot.task.exception;

import com.koinot.task.entity.User;
import com.koinot.task.payload.ApiResponseModel;
import com.koinot.task.payload.ErrorsField;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */
@Component
@Slf4j
public class ExceptionSend {



    public void senException(String msg, Exception error, User user) {
        try {
            log.error(msg,error,user);
        } catch (Exception e) {
            log.error(" my error => ", e);
        }

    }



    public HttpEntity<ApiResponseModel> errorValid(BindingResult error) {
        if (error.hasErrors()) {
            return ResponseEntity.badRequest()
                    .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value(),
                            "field",
                            error.getFieldErrors()
                                    .stream()
                                    .map(fieldError -> new ErrorsField(fieldError.getField(),
                                            fieldError.getDefaultMessage()))));
        }
        return null;
    }

}
