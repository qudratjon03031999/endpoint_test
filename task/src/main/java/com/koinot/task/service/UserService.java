package com.koinot.task.service;

import com.koinot.task.entity.Role;
import com.koinot.task.entity.User;
import com.koinot.task.enums.RoleName;
import com.koinot.task.payload.ApiResponseModel;
import com.koinot.task.payload.DataPageable;
import com.koinot.task.payload.ErrorsField;
import com.koinot.task.payload.ReqUserData;
import com.koinot.task.repository.RoleRepository;
import com.koinot.task.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */
@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    AttachmentService attachmentService;



    public HttpEntity<?> register(ReqUserData reqSignUp) {

        User user = new User();

        String info = "";

        if (reqSignUp.getId() == null) {
            if (userRepository.existsByEmail(reqSignUp.getEmail())) {
                return ResponseEntity.badRequest()
                        .body(
                                new ApiResponseModel(
                                        HttpStatus.CONFLICT.value(),
                                        "field",
                                        List.of(new ErrorsField("email", "this email is busy"))));
            }
            if (userRepository.existsByPhoneNumber(reqSignUp.getPhoneNumber())) {
                return ResponseEntity.badRequest()
                        .body(
                                new ApiResponseModel(
                                        HttpStatus.CONFLICT.value(),
                                        "field",
                                        List.of(new ErrorsField("phoneNumber", "this phoneNumber is busy"))));
            }
            if (userRepository.existsByPassportNumber(reqSignUp.getPassportNumber())) {
                return ResponseEntity.badRequest()
                        .body(
                                new ApiResponseModel(
                                        HttpStatus.CONFLICT.value(),
                                        "field",
                                        List.of(new ErrorsField("passportNumber", "this passportNumber is busy"))));
            }
            info = "create";
        } else {
            Optional<User> userOptional = userRepository.findById(reqSignUp.getId());
            if (userOptional.isPresent()) {
                if (userRepository.existsByEmailAndIdNot(reqSignUp.getEmail(), reqSignUp.getId())) {
                    return ResponseEntity.badRequest()
                            .body(
                                    new ApiResponseModel(
                                            HttpStatus.CONFLICT.value(),
                                            "field",
                                            List.of(new ErrorsField("email", "this email is busy"))));
                }
                if (userRepository.existsByPhoneNumberAndIdNot(reqSignUp.getPhoneNumber(), reqSignUp.getId())) {
                    return ResponseEntity.badRequest()
                            .body(
                                    new ApiResponseModel(
                                            HttpStatus.CONFLICT.value(),
                                            "field",
                                            List.of(new ErrorsField("phoneNumber", "this phoneNumber is busy"))));
                }
                if (userRepository.existsByPassportNumberAndIdNot(reqSignUp.getPassportNumber(), reqSignUp.getId())) {
                    return ResponseEntity.badRequest()
                            .body(
                                    new ApiResponseModel(
                                            HttpStatus.CONFLICT.value(),
                                            "field",
                                            List.of(new ErrorsField("passportNumber", "this passportNumber is busy"))));
                }
                user = userOptional.get();
            } else {
                return ResponseEntity.badRequest()
                        .body(new ApiResponseModel(HttpStatus.CONFLICT.value(), "not found this id", null));
            }
            info = "edit";
        }

        user.setFirstName(reqSignUp.getFirstName().substring(0,
                1).toUpperCase() + reqSignUp.getFirstName().substring(1));
        user.setLastName(reqSignUp.getLastName().substring(0, 1).toUpperCase() + reqSignUp.getLastName().substring(1));
        user.setMiddleName(reqSignUp.getMiddleName());
        user.setAddress(reqSignUp.getAddress());
        user.setDateOfBirth(reqSignUp.getDateOfBirth());
        user.setPhoneNumber(reqSignUp.getPhoneNumber());
        user.setEmail(reqSignUp.getEmail());
        user.setBalance(reqSignUp.getBalance());
        user.setUsername(reqSignUp.getUsername());
        user.setLanguage(reqSignUp.getLanguage());
        user.setAddress(reqSignUp.getAddress());
        user.setLongitude(reqSignUp.getLongitude());
        user.setLatitude(reqSignUp.getLatitude());
        user.setAccountNonExpired(reqSignUp.isAccountNonExpired());
        user.setAccountNonLocked(reqSignUp.isAccountNonLocked());
        user.setCredentialsNonExpired(reqSignUp.isCredentialsNonExpired());
        user.setEnabled(reqSignUp.isEnabled());
        user.setRoles(roleRepository.findAllByNameIn(reqSignUp.getRoles()));
        userRepository.save(user);
        return ResponseEntity.status(HttpStatus.OK)
                .body(new ApiResponseModel(HttpStatus.OK.value(), info, makeReqUser(user)));
    }


    public HttpEntity<?> getPageable(
            Optional<Integer> page,
            Optional<String> sortType, Optional<Integer> size,
            Optional<String> sortBy,
            Optional<String> search,
            RoleName role) {

        Sort.Direction sort = sortType.orElse("DESC").equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC;
        try {
            DataPageable userPageable = new DataPageable();
            Page<User> id;

            if (search.isEmpty() || search.get().equals("")) {
                id = userRepository.findByRolesId(roleRepository.findByName(role).getId(), PageRequest.of(
                        page.orElse(0), size.orElse(5), sort, sortBy.orElse("id")));
            } else {

                String s = search.get();
                id = userRepository
                        .findAllByPhoneNumberIgnoreCaseContainingAndFirstNameIgnoreCaseContainingAndLastNameIgnoreCaseContaining(
                                s,
                                s,
                                s,
                                PageRequest.of(
                                        page.orElse(0), size.orElse(5), sort, sortBy.orElse("id")));

            }
            userPageable.setPageable(id.getPageable());
            userPageable.setEmpty(id.isEmpty());
            userPageable.setSort(id.getSort());
            userPageable.setFirst(id.isFirst());
            userPageable.setLast(id.isLast());
            userPageable.setNumber(id.getNumber());
            userPageable.setSize(id.getSize());
            userPageable.setTotalElements(id.getTotalElements());
            userPageable.setTotalPages(id.getTotalPages());
            userPageable.setNumberOfElements(id.getNumberOfElements());
            userPageable.setContent(
                    id.stream()
                            .map(
                                    this::makeReqUser
                            )
                            .collect(Collectors.toList()));
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new ApiResponseModel(HttpStatus.OK.value(), "users", userPageable));
        } catch (Exception e) {
            return ResponseEntity.badRequest()
                    .body(
                            new ApiResponseModel(
                                    HttpStatus.CONFLICT.value(),
                                    "The request is incorrect",
                                    List.of(new ErrorsField("error", e.getMessage()))));
        }
    }


    public HttpEntity<?> getUserById(Integer id) {
        try {
            Optional<User> byId = userRepository.findById(id);
            if (byId.isPresent()) {
                User user = byId.get();
                return ResponseEntity.status(HttpStatus.OK)
                        .body(
                                new ApiResponseModel(
                                        HttpStatus.OK.value(),
                                        "user",
                                        makeReqUser(user)));
            }
            return ResponseEntity.badRequest()
                    .body(
                            new ApiResponseModel(
                                    HttpStatus.CONFLICT.value(),
                                    "not found user",
                                    List.of(new ErrorsField("error", null))));
        } catch (Exception e) {
            return ResponseEntity.badRequest()
                    .body(
                            new ApiResponseModel(
                                    HttpStatus.CONFLICT.value(),
                                    "The request is incorrect",
                                    List.of(new ErrorsField("error", e.getMessage()))));
        }
    }

    public ReqUserData makeReqUser(User user) {
        return new ReqUserData(
                user.getId(),
                user.getPhoneNumber(),
                user.getIpAddress(),
                user.getEmail(),
                user.getExpiredCode(),
                user.getVerifyCode(),
                user.getUsername(),
                user.getBalance(),
                user.getFirstName(),
                user.getLastName(),
                user.getDateOfBirth(),
                user.getMiddleName(),
                user.getRegion(),
                user.getPassportNumber(),
                user.getLanguage(),
                user.getAddress(),
                user.getLongitude(),
                user.getLatitude(),
                attachmentService.generateFile(user.getPhoto()),
                user.getRoles().stream().map(Role::getName).collect(
                        Collectors.toList()),
                user.isAccountNonExpired(),
                user.isAccountNonLocked(),
                user.isCredentialsNonExpired(),
                user.isEnabled()
        );
    }
}
