package com.koinot.task.entity;

import com.koinot.task.enums.AttachmentTypeEnumWhy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "attachment")
public class Attachment {

    @Id
    @SequenceGenerator(
            name = "attachment_id_sequence",
            sequenceName = "attachment_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "attachment_id_sequence"
    )
    private Integer id;

    @OrderBy
    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private Timestamp createdAt;

    @UpdateTimestamp
    @Column(nullable = false)
    private Timestamp updatedAt;

    @CreatedBy
    @Column(updatable = false)
    private Long createdBy;

    @LastModifiedBy
    private Long updatedBy;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String name;

    private String urlTelegram;

    private long size;

    private String contentType;

    private String photo;

    private String pathOriginal;

    private String pathAverage;

    private String pathLow;

    private String extension;

    @ManyToOne
    private User user;

    @Enumerated(value = EnumType.STRING)
    private AttachmentTypeEnumWhy why;


}
