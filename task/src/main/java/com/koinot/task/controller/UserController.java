package com.koinot.task.controller;

import com.koinot.task.entity.User;
import com.koinot.task.enums.RoleName;
import com.koinot.task.exception.ExceptionSend;
import com.koinot.task.payload.ApiResponseModel;
import com.koinot.task.payload.ReqUserData;
import com.koinot.task.sercurity.CurrentUser;
import com.koinot.task.service.UserService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.Optional;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */
@RestController
@RequestMapping("/api/user/v1")
@PreAuthorize("hasAnyAuthority('ROLE_SUPPER_ADMIN')")
@ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
@ApiResponses(value = {
        @ApiResponse(code = 409, message = "your request not suitable"),
        @ApiResponse(code = 200, message = "successes"),
        @ApiResponse(code = 403, message = "you are not allowed or haven't permission"),})
@Slf4j
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    ExceptionSend exceptionSend;


    //**************** REGISTER USER ****************//
    @ApiOperation(" user register for admin ")
    @PostMapping("/register")
    @Validated
    public HttpEntity<?> register(@Valid @RequestBody ReqUserData reqUser, BindingResult error){
        return error.hasErrors() ? exceptionSend.errorValid(error) : userService.register(reqUser);
    }



    // **************** GET USER BY ID ****************//
    @ApiOperation(" get user by id")
    @GetMapping("/getUserById/{id}")
    public HttpEntity<?> getUserById(@PathVariable Integer id) {
        return userService.getUserById(id);
    }

    // **************  GET USERS  ****************//
    @ApiOperation(" get users for admin ")
    @GetMapping()
    public HttpEntity<?> findPaginated(
            @Valid @RequestParam(value = "page", defaultValue = "0") @Min(0) Optional<Integer> page,
            @Valid @RequestParam(value = "size", defaultValue = "5") @Min(0) Optional<Integer> size,
            @Valid @RequestParam(value = "sortBy", defaultValue = "id") Optional<String> sortBy,
            @Valid @RequestParam(value = "role", defaultValue = "ROLE_MODERATOR") RoleName role,
            @Valid @RequestParam(value = "sortType", defaultValue = "DESC") Optional<String> sortType,
            @Valid @RequestParam(value = "search" ,defaultValue = "") Optional<String> search) {

        return userService.getPageable(page,sortType, size, sortBy, search,role);

    }

    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public HttpEntity<ApiResponseModel> handleException(@ApiIgnore @CurrentUser User user, Exception e) {
        exceptionSend.senException(" AuthController handleException => ", e, user);
        log.error(" AuthController handleException => ", e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value(), e.getMessage(), e.hashCode()));
    }
}
