package com.koinot.task.controller;


import com.koinot.task.entity.User;
import com.koinot.task.enums.QualityType;
import com.koinot.task.payload.ApiResponseModel;
import com.koinot.task.sercurity.CurrentUser;
import com.koinot.task.service.AttachmentService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */
@Slf4j
@RestController
@RequestMapping("/koinot/attachment/v1")
public class AttachmentController {

    @Autowired
    AttachmentService attachmentService;

    @PostMapping("/userProfile")
    public HttpEntity<ApiResponseModel> userProfile(
            MultipartHttpServletRequest request,
            @ApiIgnore @CurrentUser User user, @RequestParam(required = false, defaultValue = "null") String name) {
        return attachmentService.userProfile(request, user, name);
    }



    @GetMapping("/byteFile/{id}")
    @ApiOperation("QualityType values: ORIGINAL, AVERAGE, LOW")
    public HttpEntity<?> byteFileQuality(
            @PathVariable(required = false) Integer id,
            @RequestParam(required = false, defaultValue = "ORIGINAL") QualityType type) throws IOException {
        return attachmentService.byteFileQuality(id, type);
    }


    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public HttpEntity<ApiResponseModel> handleException(@ApiIgnore @CurrentUser User user, Exception e) {
        log.error(" AuthController handleException => ", e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value(), e.getMessage(), e.hashCode()));
    }
}
