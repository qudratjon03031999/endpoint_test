package com.koinot.task.controller;

import com.koinot.task.repository.UserRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */
@Controller
@Setter
@Getter
public class web {
    @Autowired UserRepository userRepository;


    @GetMapping("/user")
    public String user(Model model) {
        model.addAttribute("user",userRepository.findAll());
        model.addAttribute("message",userRepository.count());
        model.addAttribute("statistic",userRepository.generationStatistic());
        return "Users";
    }

    @GetMapping("/home")
    public String home() {
        return "home";
    }

}
