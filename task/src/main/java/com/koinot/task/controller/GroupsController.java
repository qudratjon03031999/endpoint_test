package com.koinot.task.controller;

import com.koinot.task.entity.User;
import com.koinot.task.enums.RoleName;
import com.koinot.task.exception.ExceptionSend;
import com.koinot.task.payload.ApiResponseModel;
import com.koinot.task.payload.ReqUserData;
import com.koinot.task.sercurity.CurrentUser;
import com.koinot.task.service.GroupService;
import com.koinot.task.service.UserService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.Optional;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */
@RestController
@RequestMapping("/api/user/v1")
@PreAuthorize("hasAnyAuthority('ROLE_SUPPER_ADMIN')")
@ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
@ApiResponses(value = {
        @ApiResponse(code = 409, message = "your request not suitable"),
        @ApiResponse(code = 200, message = "successes"),
        @ApiResponse(code = 403, message = "you are not allowed or haven't permission"),})
@Slf4j
public class GroupsController {

    @Autowired
    GroupService groupService;

    @Autowired
    ExceptionSend exceptionSend;


    //**************** REGISTER USER ****************//
    @ApiOperation(" create group")
    @PostMapping("/addGroup/{name}")
    public HttpEntity<?> register(@PathVariable String name){
        return groupService.addGroup(name);
    }


    // **************** GET USER BY ID ****************//
    @ApiOperation(" add to group user by id")
    @GetMapping("/adduserToGroup/{id}")
    public HttpEntity<?> adduserToGroup(@PathVariable Integer id) {
        return groupService.adduserToGroup(id);
    }


    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public HttpEntity<ApiResponseModel> handleException(@ApiIgnore @CurrentUser User user, Exception e) {
        exceptionSend.senException(" AuthController handleException => ", e, user);
        log.error(" AuthController handleException => ", e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value(), e.getMessage(), e.hashCode()));
    }
}
