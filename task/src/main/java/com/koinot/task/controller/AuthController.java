package com.koinot.task.controller;

import com.koinot.task.entity.User;
import com.koinot.task.exception.ExceptionSend;
import com.koinot.task.payload.ApiResponseModel;
import com.koinot.task.payload.ReqUser;
import com.koinot.task.payload.ReqUserProfile;
import com.koinot.task.sercurity.AuthService;
import com.koinot.task.sercurity.CurrentUser;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import java.text.ParseException;

/**
 * @description: TODO
 * @projectName endpoint
 * @since 17/03/22
 * @link Telegram Link https://t.me/qudratjon03031999
 * @author: Qudratjon Komilov
 */

@RestController
@RequestMapping("/api/auth/v1")
@ApiResponses(value = {
        @ApiResponse(code = 409, message = "your request not suitable"),
        @ApiResponse(code = 200, message = "successes"),
        @ApiResponse(code = 403, message = "you are not allowed"),})
@Slf4j
public class AuthController {


    @Autowired
    AuthService authService;

    @Autowired
    ExceptionSend exceptionSend;


    //**************** REGISTER USER ****************//
    @PermitAll
    @ApiOperation("this is user register ")
    @PostMapping("/register")
    @Validated
    public HttpEntity<ApiResponseModel> register(@Valid @RequestBody ReqUser reqUser, BindingResult error) {
        return error.hasErrors() ? exceptionSend.errorValid(error) : authService.register(reqUser);
    }

    //**************** LOGIN USER ****************//
    @PermitAll
    @PostMapping("/login")
    @ApiOperation("phone number and password")
    @Validated
    public HttpEntity<ApiResponseModel> login(@Valid @RequestBody ReqUser reqUser, BindingResult error) {
        return error.hasErrors() ? exceptionSend.errorValid(error) : authService.login(reqUser);
    }

    //**************** SEND CODE  ****************//
    @ApiOperation("if you wand send code to the phone number")
    @PostMapping("/sendCode/{phoneNumber}")
    public HttpEntity<ApiResponseModel> sendCode(@PathVariable String phoneNumber) {

        return authService.sendCode(phoneNumber);

    }


    //**************** VERIFY USER ****************//
    @ApiOperation("verification for user, after verification get token")
    @PostMapping("/verify/{code}/{phoneNumber}")
    public HttpEntity<ApiResponseModel> verify(@PathVariable String code, @PathVariable String phoneNumber) {
        return authService.verify(code, phoneNumber);
    }


    //**************** NEW PASSWORD ****************//
    @PostMapping("/newPassword/{password}")
    @ApiOperation("change password")
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    public HttpEntity<ApiResponseModel> newPassword(@PathVariable String password, @ApiIgnore @CurrentUser User user) {
        return authService.newPassword(password, user);
    }


    // **************** GET  YOURSELF  ****************//
    @GetMapping("/me")
    @ApiOperation("get user data")
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    public HttpEntity<ApiResponseModel> getUser(@ApiIgnore @CurrentUser User user) {
        if (user == null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body(new ApiResponseModel(HttpStatus.FORBIDDEN.value(), "forbidden", null));
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .body(new ApiResponseModel(HttpStatus.ACCEPTED.value(), "user info", user));
    }

    // **************** GET  YOURSELF  ****************//
    @PostMapping("/editMe")
    @ApiOperation("change account")
    @Validated
    @ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
    public HttpEntity<ApiResponseModel> editMe(@ApiIgnore @CurrentUser User user,
                                               @Valid @RequestBody ReqUserProfile forEdit,
                                               BindingResult error) throws ParseException {
        return error.hasErrors() ? exceptionSend.errorValid(error) : authService.editMe(user, forEdit);
    }


    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public HttpEntity<ApiResponseModel> handleException(@ApiIgnore @CurrentUser User user, Exception e) {
        exceptionSend.senException(" AuthController handleException => ", e, user);
        log.error(" AuthController handleException => ", e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value(), e.getMessage(), e.hashCode()));
    }
}
